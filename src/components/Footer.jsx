import React from 'react';

import './Footer.css';

const Footer = (props) => (
  <footer className="footer">
    <div className="container">
      <small className="text-muted">
        <span>© Copyright 2017 <a href="#foot">Prenetics.local</a>.</span>
      </small>
    </div>
  </footer>
)

export default Footer
