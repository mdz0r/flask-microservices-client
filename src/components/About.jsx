import React from 'react';

const About = () => (
  <div>
    <h1>About</h1>
    <hr/><br/>
    <p>MicroService assignment - Prenetics</p>
  </div>
)

export default About;
